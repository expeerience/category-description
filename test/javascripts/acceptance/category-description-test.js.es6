import { acceptance } from "discourse/tests/helpers/qunit-helpers";

acceptance("CategoryDescription", { loggedIn: true });

test("CategoryDescription works", async assert => {
  await visit("/admin/plugins/category-description");

  assert.ok(false, "it shows the CategoryDescription button");
});
