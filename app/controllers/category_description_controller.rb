class CategoryDescriptionController < ::ApplicationController
  # requires_plugin CategoryDescription

  # before_action :ensure_logged_in

  def index
  end

  def isDescription
    topic = Topic.find_by(id: params[:topic_id], category_id: params[:category_id], archetype: "about");
    if topic then
      render json: { has_description: true}
    else
      render json: { has_description: false}
    end
  end

  def hasDescription
    topic = Topic.find_by(category_id: params[:category_id], archetype: "about");
    if topic then
      render json: { has_description: true}
    else
      render json: { has_description: false}
    end
  end

  def generateDescription
    category = Category.find_by_id(params[:category_id]);
    user = User.find_by_id(params[:user_id]);
    
    if category then
      topic = Topic.find_by(category_id: category.id, archetype: "about");
      if topic then
        puts "ABOUT TOPIC FOUND. NOT DOING ANYTHING."
      else
        puts "NO ABOUT TOPIC FOUND. CREATING."
        Topic.transaction do
          t = Topic.new(title: "About " + category.name, user: user, pinned_at: Time.now, category_id: category.id)
          t.visible = false
          t.skip_callbacks = true
          t.ignore_category_auto_close = true
          t.delete_topic_timer(TopicTimer.types[:close])
          t.archetype = "about"
          t.save!(validate: false)
          category.update_column(:topic_id, t.id)
          post = t.posts.build(raw: category.description || category.post_template, user: user)
          post.save!(validate: false)
          category.update_column(:description, post.cooked) if category.description.present?
          t
        end
      end
    else
      puts "NO CATEGORY FOUND. CANNOT DO ANYTHING."
    end


    render json: { }
  end    
end
