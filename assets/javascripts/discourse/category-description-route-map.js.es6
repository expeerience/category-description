export default function() {
  this.route("category-description", function() {
    this.route("actions", function() {
      this.route("show", { path: "/:id" });
    });
  });
};
