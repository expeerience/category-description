import { getOwner } from 'discourse-common/lib/get-owner';
import showModal from 'discourse/lib/show-modal';
import { ajax } from 'discourse/lib/ajax';

export default {
  hasDescription: true,

  shouldRender(args, component) {
    return Discourse.User.currentProp('staff');
  },

  shasDescription(){
    console.log("HAS DESCRIPTION?")
    return this.has_description;
  },

  actions: {
    createDescription(){
      console.log("currentUser: ")
      console.log(this.get('currentUser').id);

      if(this.category) {
        console.log("Calling back end.");
        ajax(`/category/generate-description`, {
          type: 'PUT',
          data: {
            category_id: this.category.id,
            // type,
            user_id: this.get('currentUser').id
          }
        // }).then((result) => {
        //   if (result.success) {
        //     // this.updateTopic(user.username, action, type);
        //   }
        // }).catch(popupAjaxError).finally(() => {
        //   // this.set(`${type}Saving`, false);
        });
      } else{
        console.log("No category found. Nothing done.");
      }


  
    },
  }
};
