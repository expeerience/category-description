import { withPluginApi } from "discourse/lib/plugin-api";

function initializeCategoryDescription(api) {
  // https://github.com/discourse/discourse/blob/master/app/assets/javascripts/discourse/lib/plugin-api.js.es6
}

export default {
  name: "category-description",

  initialize() {
    withPluginApi("0.8.31", initializeCategoryDescription);
    // alert("This is an alert test on initialization.");
    console.log("LOG This is a message on initialization.");
  }
};
