require_dependency "category_description_constraint"

CategoryDescription::Engine.routes.draw do
  get "/" => "category_description#index", constraints: CategoryDescriptionConstraint.new
  get "/actions" => "actions#index", constraints: CategoryDescriptionConstraint.new
  get "/actions/:id" => "actions#show", constraints: CategoryDescriptionConstraint.new
  # put "/category/generate_description" => "category_description#generateDescription", constraints: CategoryDescriptionConstraint.new
end
