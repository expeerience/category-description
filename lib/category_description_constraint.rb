class CategoryDescriptionConstraint
  def matches?(request)
    SiteSetting.category_description_enabled
  end
end
