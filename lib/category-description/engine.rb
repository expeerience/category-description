module CategoryDescription
  class Engine < ::Rails::Engine
    engine_name "CategoryDescription".freeze
    isolate_namespace CategoryDescription

    config.after_initialize do
      Discourse::Application.routes.append do
        mount ::CategoryDescription::Engine, at: "/category-description"
      end
    end
  end
end
