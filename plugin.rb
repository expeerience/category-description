# frozen_string_literal: true

# name: CategoryDescription
# about: Creates a Category description Topic
# version: 0.1
# authors: frank3manuel@gmail.com
# url: https://github.com/frank3manuel@gmail.com

register_asset 'stylesheets/common/category-description.scss'
register_asset 'stylesheets/desktop/category-description.scss', :desktop
register_asset 'stylesheets/mobile/category-description.scss', :mobile

enabled_site_setting :category_description_enabled

PLUGIN_NAME ||= 'CategoryDescription'

# load File.expand_path('lib/category-description/engine.rb', __dir__)

after_initialize do
  # https://github.com/discourse/discourse/blob/master/lib/plugin/instance.rb
  # load File.expand_path('../app/controllers/actions_controller.rb', __FILE__)
  load File.expand_path('../app/controllers/category_description_controller.rb', __FILE__)

  Discourse::Application.routes.append do
    # get '/category' => 'notebook#index'
    # put '/notes/:note_id' => 'notes#update'

    get '/topic/isDescription' => 'category_description#isDescription'
    get "/category/hasDescription" => "category_description#hasDescription"
    put "/category/generate-description" => "category_description#generateDescription"

  end

  DiscourseEvent.on(:category_created) do |category|
    puts "DISCOURSE EVENT DISCOURSE EVENT DISCOURSE EVENT category_created DISCOURSE EVENT DISCOURSE EVENT DISCOURSE EVENT"
    puts "Category created with ID: #{category.id}, name: #{category.name} "
    topic = Topic.find_by(category_id: category.id, archetype: "about");
    if topic then
      puts "ABOUT TOPIC FOUND. NOT DOING ANYTHING."
    else
      puts "NO ABOUT TOPIC FOUND. CREATING."
      about_topic_template = I18n.t("site_settings.category.about_topic_post_template", about_topic_replace_paragraph: I18n.t("site_settings.category.about_topic_replace_paragraph"))

      Topic.transaction do
        t = Topic.new(title: I18n.t("site_settings.category.about_topic_prefix", category: category.name), user: category.user, pinned_at: Time.now, category_id: category.id)
        # t = Topic.new(title: "About " + category.name, user: category.user, pinned_at: Time.now, category_id: category.id)
        t.visible = false
        t.skip_callbacks = true
        t.ignore_category_auto_close = true
        t.delete_topic_timer(TopicTimer.types[:close])
        t.archetype = "about"
        t.save!(validate: false)
        category.update_column(:topic_id, t.id)
        post = t.posts.build(raw: category.description || about_topic_template, user: category.user)
        post.save!(validate: false)
        category.update_column(:description, post.cooked) if category.description.present?
        category.update_column(:subcategory_list_style, 'boxes_with_featured_topics')
        category.update_column(:show_subcategory_list, true)
      end
    end

  end
  
end
